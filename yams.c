/**
 * 
 * \brief Le jeu du Yams, programmé en C
 * 
 * \author LAGANE Antoine
 * 
 * \version 1.0
 * 
 * \date 23 novembre 2021
 * 
 * Le fameux jeu du Yams, programmé en C ! Venez vous amuser en famille ou entre amis à lancer des dés !
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

/**
 * 
 * \def NB_TOUR
 * 
 * \brief Nombre de tour avant la fin de la partie
 * 
 */
const int NB_TOUR = 13;

/**
 * 
 * \def NB_JOUEUR
 * 
 * \brief Nombre de joueur dans la partie
 * 
 */
const int NB_JOUEUR = 2;

/**
 * 
 * \def MAX_SCORE_DE
 * 
 * \brief Score maximal que le dé puisse atteindre
 * 
 */
const int MAX_SCORE_DE = 6;

/**
 * 
 * \def NB_CASE
 * 
 * \brief Nombre de case que un seul joueur doit remplir par partie
 * 
 */
#define NB_CASE 17

/**
 * 
 * \typedef tabScore
 * 
 * \brief Type tableau de NB_CASE entiers
 * 
 * Ce tableau sert à stocker les scores obtenus par un seul joueur durant toue la partie
 * 
 */
typedef int tabScore[NB_CASE];

/**
 * 
 * \def NB_DE
 * 
 * \brief Nombre de dé présent dans le jeu
 * 
 */
#define NB_DE 5

/**
 * 
 * \typedef tabDe
 * 
 * \brief Type tableau de NB_DE entiers
 * 
 * Ce tableau sert à stocker les scores des dés à chaques tour
 * 
 */
typedef int tabDe[NB_DE];

/**
 * 
 * \fn void initTabDe(tabDe tDe)
 * 
 * \brief Initialise à 0 toutes les cases du tableau de dé
 * 
 * \param tDe : le tableau avec les dés à initialiser
 * 
 * Permet de remettre les scores des dé à 0 à chaque lancer
 * 
 */
void initTabDe(tabDe tDe) {

    int i;

    for(i = 0;i < NB_DE; i++) {

        tDe[i] = 0;

    }

}

/**
 * 
 * \fn void initTabScore(tabScore tScore)
 * 
 * \brief Initialise à 0 toutes les cases du tableau de score
 * 
 * \param tDe : le tableau avec les scores à initialiser
 * 
 * Permet de remettre les scores à 0 à chaque début de partie
 * 
 */
void initTabScore(tabScore tScore) {

    int i;

    for(i = 0;i < NB_CASE; i++) {

        tScore[i] = 0;

    }

}

/**
 * 
 * \fn int aleaInfBorne()
 * 
 * \brief Envoie un nombre aléatoire entre 1 et 6
 * 
 * \return le score du dé 
 * 
 * Permet de lancer les dés aléatoirement
 * 
 */
int aleaInfBorne() {

    srand(time(NULL));

    while (rand() == 0) {

        srand(time(NULL));
        
    }
    
    return ((rand()%(MAX_SCORE_DE + 1 - 1)) + 1);

}

/**
 * 
 * \fn void nomJoueur(char j1[10], char j2[10])
 * 
 * \brief Demande le nom de chaques joueurs
 * 
 * \param j1 : Nom du joueur 1
 * 
 * \param j2 : Nom du joueur 2
 * 
 * Demande le nom de chacun des 2 joueurs
 * 
 */
void nomJoueur(char j1[10], char j2[10]) {

    printf("Quel est le nom du Joueur 1 (10 caractères maximum)\n");
    scanf("%s", j1);

    printf("Quel est le nom du Joueur 2 (10 caractères maximum)\n");
    scanf("%s", j2);

}

/**
 * 
 * \fn void afficheTableau1Joueur(tabScore tscore, char joueurActuel[10])
 * 
 * \brief Affiche le tableau des scores d'un seul des joueurs
 * 
 * \param tscore : Tableau des scores du joueur en train de jouer
 * 
 * \param joueurActuel : Nom du joueur en train de jouer
 * 
 * Donne le tableau des scores du joueur en train de jouer pour qu'il puisse voir ses scores et choisir où il affecte son score
 * 
 */
void afficheTableau1Joueur(tabScore tscore, char joueurActuel[10]) {

    printf("//|////////////////|%10s|\n", joueurActuel);
    printf("--|----------------|----------|\n");
    printf("01|1 [Total de 1]  |    %2d    |\n", tscore[0]);
    printf("02|2 [Total de 2]  |    %2d    |\n", tscore[1]);
    printf("03|3 [Total de 3]  |    %2d    |\n", tscore[2]);
    printf("04|4 [Total de 4]  |    %2d    |\n", tscore[3]);
    printf("05|5 [Total de 5]  |    %2d    |\n", tscore[4]);
    printf("06|6 [Total de 6]  |    %2d    |\n", tscore[5]);
    printf("//|----------------|----------|\n");
    printf("//|Bonus si >62[35]|    %2d    |\n", tscore[6]);
    printf("//|Total supérieur |    %2d    |\n", tscore[7]);
    printf("--|----------------|----------|\n");
    printf("07|Brelan   [total]|    %2d    |\n", tscore[8]);
    printf("08|Carré    [total]|    %2d    |\n", tscore[9]);
    printf("09|Full House  [25]|    %2d    |\n", tscore[10]);
    printf("10|Petite Suite[30]|    %2d    |\n", tscore[11]);
    printf("11|Grande suite[40]|    %2d    |\n", tscore[12]);
    printf("12|Yams        [50]|    %2d    |\n", tscore[13]);
    printf("13|Chance   [total]|    %2d    |\n", tscore[14]);
    printf("--|----------------|----------|\n");
    printf("//|Total inférieur |    %2d    |\n", tscore[15]);
    printf("//|Total           |    %2d    |\n", tscore[16]);
    printf("--|----------------|----------|\n");
}

/**
 * 
 * \fn void afficheTableau(tabScore tscore1, tabScore tscore2, char j1[10], char j2[10])
 * 
 * \brief Affiche le tableau de tous les scores de tous les joueurs
 * 
 * \param tscore1 : Tableau des scores du joueur 1
 * 
 * \param tscore2 : Tableau des scores du joueur 2
 * 
 * \param j1 : Nom du joueur 1
 * 
 * \param j2 : Nom du joueur 2
 * 
 * Affiche tous les scores des joueurs pour voir l'avancement des scores de chacun et savoir qui est en train de gagner/perdre
 * 
 */
void afficheTableau(tabScore tscore1, tabScore tscore2, char j1[10], char j2[10]) {

    printf("////////////////|%10s|%10s|\n", j1, j2);
    printf("----------------|----------|----------|\n");
    printf("1 [Total de 1]  |    %2d    |	%2d    |\n", tscore1[0], tscore2[0]);
    printf("2 [Total de 2]  |    %2d    |	%2d    |\n", tscore1[1], tscore2[1]);
    printf("3 [Total de 3]  |    %2d    |	%2d    |\n", tscore1[2], tscore2[2]);
    printf("4 [Total de 4]  |    %2d    |	%2d    |\n", tscore1[3], tscore2[3]);
    printf("5 [Total de 5]  |    %2d    |	%2d    |\n", tscore1[4], tscore2[4]);
    printf("6 [Total de 6]  |    %2d    |	%2d    |\n", tscore1[5], tscore2[5]);
    printf("----------------|----------|----------|\n");
    printf("Bonus si >62[35]|    %2d    |	%2d    |\n", tscore1[6], tscore2[6]);
    printf("Total supérieur |    %2d    |    %2d    |\n", tscore1[7], tscore2[7]);
    printf("----------------|----------|----------|\n");
    printf("Brelan   [total]|    %2d    |	%2d    |\n", tscore1[8], tscore2[8]);
    printf("Carré    [total]|    %2d    |	%2d    |\n", tscore1[9], tscore2[9]);
    printf("Full House  [25]|    %2d    |	%2d    |\n", tscore1[10], tscore2[10]);
    printf("Petite Suite[30]|    %2d    |    %2d    |\n", tscore1[11], tscore2[11]);
    printf("Grande suite[40]|    %2d    |	%2d    |\n", tscore1[12], tscore2[12]);
    printf("Yams        [50]|    %2d    |	%2d    |\n", tscore1[13], tscore2[13]);
    printf("Chance   [total]|    %2d    |	%2d    |\n", tscore1[14], tscore2[14]);
    printf("----------------|----------|----------|\n");
    printf("Total inférieur |    %2d    |	%2d    |\n", tscore1[15], tscore2[15]);
    printf("Total           |    %2d    |    %2d    |\n", tscore1[16], tscore2[16]);
    printf("----------------|----------|----------|\n");
}

/**
 * 
 * \fn void propositionJeu(char joueurActuel[10])
 * 
 * \brief Demande au joueur si il veut jouer
 * 
 * \param joueurActuel : Nom du joueur en train de jouer
 * 
 * Demande au joueur si il veut jouer et si il ne veut pas jouer, le programme attend que le joueur saisisse 1 pour jouer
 * 
 */
void propositionJeu(char joueurActuel[10]) {

    int reponse;

    printf("%s, voulez vous jouer ? Oui(1) ou Non(0)\n", joueurActuel);
    scanf("%d", &reponse);

    while (reponse != 1 && reponse != 0)
    {
        printf("Réponse incorrecte, voulez vous jouer ? Oui(1) ou Non(2)\n");
        scanf("%d", &reponse);
    }

    while (reponse == 0)
    {
        
        printf("Très bien, saisissez 1 quand vous voudrez jouer\n");
        scanf("%d", &reponse);

        while (reponse != 1)
        {
            printf("Réponse incorrecte\n");
            printf("Très bien, saisissez 1 quand vous voudrez jouer\n");
            scanf("%d", &reponse);
        }
        
    }  

}

/**
 * 
 * \fn void choixDe(tabDe tDe, tabDe tGarde, char joueurActuel[10])
 * 
 * \brief Propose au joueur de garder un dé
 * 
 * \param tDe : Tableau avec les scores des dés lancés
 * 
 * \param tGarde : Tableau avec les scores des dés sauvegardés lors des lancés précédents
 * 
 * \param joueurActuel : Nom du joueur en train de jouer
 * 
 * Demande au joueur si il souhaite garder son dé afin qu'il reste ou le relancer au lancer suivant
 * 
 */
void choixDe(tabDe tDe, tabDe tGarde, char joueurActuel[10]) {

    int i, choix;

    for(i = 0;i < NB_DE; i++) {

        printf("Que voulez vous faire du dé N°%d (score obtenu : %d) : Garder(1) ou Relancer(2) ?\n", i, tDe[i]);
        scanf("%d", &choix);

        while (choix != 1 && choix != 2)
        {
            
            printf("Erreur lors de la saisie, réessayez \n");
            printf("Que voulez vous faire du dé N°%d : Garder(1) ou Relancer(2) ?\n", i);
            scanf("%d", &choix);

        }

        if (choix == 2) {

            tGarde[i] = 0;
            tDe[i] = 0;

        } else {

            tGarde[i] = tDe[i];

        }

    }

}

/**
 * 
 * \fn void lanceDe(char joueurActuel[10], tabDe tDe, tabDe tGarde)
 * 
 * \brief Affiche les scores de tous les dés
 * 
 * \param joueurActuel : Nom du joueur en train de jouer
 * 
 * \param tDe : Tableau avec les scores des dés lancés
 * 
 * \param tGarde : Tableau avec les scores des dés sauvegardés lors des lancés précédents
 * 
 * Demande au joueur si il veut lancer les dés et affiche le score des 5 dés pour les montrer au joueur
 * 
 */
void lanceDe(char joueurActuel[10], tabDe tDe, tabDe tGarde) {

    int nbLance, choix, de;

    initTabDe(tDe);
    initTabDe(tGarde);

    for (nbLance = 1; nbLance <= 3; nbLance++) {

        printf("%s, voulez vous lancer les dés ? Oui (1) ou Non(2)\n", joueurActuel);
        scanf("%d", &choix);

        while (choix != 1 && choix != 2)
        {
            
            printf("Erreur lors de la saisie, réessayez \n");
            printf("%s, voulez vous lancer les dés ? Oui(1) ou Non(2)\n", joueurActuel);
            scanf("%d", &choix);

        }

        if (choix == 1) {

            for (de = 0; de < NB_DE; de++) {

                tDe[de] = tGarde[de];

                if (tDe[de] == 0) {

                    tDe[de] = aleaInfBorne();
                    sleep(1);

                }

                printf("Dé n°%d : %d\t  |", de, tDe[de]);

            }

            printf("\n");

            choixDe(tDe, tGarde, joueurActuel);

        }

    }

}

/**
 * 
 * \fn int sommeDe(tabDe tDe)
 * 
 * \brief Calcule la somme de tous les dés
 * 
 * \param tDe : Tableau avec les scores des dés lancés
 * 
 * \return la somme de tous les dés
 * 
 * Calcule la somme de tous les dés du tableau
 * 
 */
int sommeDe(tabDe tDe) {

    int i, somme;

    somme = 0;

    for (i=0 ; i < NB_DE ; i++) {

        somme = somme + tDe[i];

    }

    return somme;

}

/**
 * 
 * \fn int afficheSomme(tabDe tDe)
 * 
 * \brief Affiche la somme de tous les dés
 * 
 * \param tDe : Tableau avec les scores des dés lancés
 * 
 * \return la somme de tous les dés
 * 
 * Affiche les dés et la somme de tous les dés obtenus
 * 
 */
int afficheSomme(tabDe tDe) {

    int de, somme;

    for (de = 0; de < NB_DE; de++) {

        printf("Dé n°%d : %d\t|", de, tDe[de]);

    }

    somme = sommeDe(tDe);

    printf("Somme des dés : %d\t|", somme);

    return somme;

}

/**
 * 
 * \fn void triDe(tabDe tDe)
 * 
 * \brief Trie les scores des dés dans l'ordre croissant
 * 
 * \param tDe : Tableau avec les scores des dés à trier
 * 
 * ¨Permet de trier les scores des dés dans l'ordre croissant afin de vérifier la présence de suite, de brelan ou de carré
 * 
 */
void triDe(tabDe tDe) {

    int i,j,c;

    for(i = 0 ; i < NB_DE - 1 ; i++){

        for( j = i + 1; j < NB_DE ; j++) {

            if ( tDe[i] > tDe[j] ) {

                c = tDe[i];

                tDe[i] = tDe[j];

                tDe[j] = c;

            }

        }
    
    }

}

/**
 * 
 * \fn int scoreSomme(tabDe tDe, int nbSomme)
 * 
 * \brief Calcule la somme de dé de même score
 * 
 * \param tDe : Tableau avec les scores des dés lancés
 * 
 * \param nbSomme : Score du dé à trouver dans la liste des dés
 * 
 * \return Somme de tous les dés de même score
 * 
 * Cherche tous les dés avec le score nbSomme et retourne la somme de chacun des dés avec ce score (retourne -1 si aucun des dés cherché n'est présent)
 * 
 */
int scoreSomme(tabDe tDe, int nbSomme) {

    int somme, i;

    somme = 0;

    for (i = 0; i < NB_DE; i++) {
        
        if (tDe[i] == nbSomme) {

            somme = somme + nbSomme;

        }

    }

    if (somme == 0) {

        somme = -1;

    }
    
    return somme;

}

/**
 * 
 * \fn int brelan(tabDe tDe)
 * 
 * \brief Vérifie si les dés obtenu forme un brelan
 * 
 * \param tDe : Tableau avec les scores des dés lancés
 * 
 * \return Somme des 3 dés formant le brelan
 * 
 * Cherche dans le tableau des dés si il y a un brelan et retourne le score obtenu (si il n'y a pas de brelan, on retourne -1)
 * 
 */
int brelan(tabDe tDe) {

    int score;

    triDe(tDe);

    if(tDe[0] == tDe[2]) {

        score = tDe[0] * 3;

    } else if (tDe[1] == tDe[3]) {

        score = tDe[1] * 3;
 
    } else if (tDe[2] == tDe[4]) {

        score = tDe[2] * 3;

    } else {

        score = -1;

    }

    return score;

}

/**
 * 
 * \fn int carre(tabDe tDe)
 * 
 * \brief Vérifie si les dés obtenu forme un carré
 * 
 * \param tDe : Tableau avec les scores des dés lancés
 * 
 * \return Somme des 4 dés formant le carré
 * 
 * Cherche dans le tableau des dés si il y a un carré et retourne le score obtenu (si il n'y a pas de carré, on retourne -1)
 * 
 */
int carre(tabDe tDe) {
   
    int score;

    triDe(tDe);

    if(tDe[0] == tDe[3]) {

        score = tDe[0] * 4;

    } else if (tDe[1] == tDe[4]) {

        score = tDe[1] * 4;
 
    } else {

        score = -1;

    }

    return score;

}

/**
 * 
 * \fn bool fullHouse(tabDe tDe)
 * 
 * \brief Vérifie la présence d'un full dans les dés obtenus
 * 
 * \param tDe : Tableau avec les scores des dés lancés
 * 
 * \return vrai si les dés obtenus forment un full, faux sinon
 * 
 * Cherche dans le tableau des dés la présence d'un brelan, puis regarde ensuite si les 2 autres dés non-compris dans le brelan sont identique et retourne si oui ou non il y a un full
 * 
 */
bool fullHouse(tabDe tDe) {

    int tSuite[2];
    bool trouve;
    int sommeBrelan, scoreBrelan, i1, i2;

    trouve = false;
    sommeBrelan = brelan(tDe);
    i2 = 0;

    if (sommeBrelan != -1) {

        scoreBrelan = sommeBrelan / 3;

        for(i1 = 0 ; i1 < NB_DE ; i1++) {

            if(tDe[i1] != scoreBrelan) {

                tSuite[i2] = tDe[i1];  

                i2 = i2 + 1;               

            }

        }

        if(tSuite[0] == tSuite[1]) {

            trouve = true;

        }

    }

    return trouve;

}

/**
 * 
 * \fn bool petiteSuite(tabDe tDe)
 * 
 * \brief Vérifie la présence d'une petite suite dans les dés obtenus
 * 
 * \param tDe : Tableau avec les scores des dés lancés
 * 
 * \return vrai si les dés obtenus forment une petite suite, faux sinon
 * 
 * Cherche dans le tableau des dés la présence d'une petite suite et retourne si oui ou non il y a une petite suite
 * 
 */
bool petiteSuite(tabDe tDe) {

    bool trouve;
    int i, deEtud, deActuel, avance;

    triDe(tDe);

    trouve = false;
    i = 1;
    deActuel = tDe[0];
    avance = 1;

    while(i < NB_DE && trouve == false) {

        deEtud = tDe[i];

        if(deActuel + 1 == deEtud) {

            deActuel = tDe[i];
            avance = avance + 1;

        }

        if(avance >= 4) {

            trouve = true;

        }

        i = i + 1;

    }

    return trouve;

}

/**
 * 
 * \fn bool grandeSuite(tabDe tDe)
 * 
 * \brief Vérifie la présence d'une grande suite dans les dés obtenus
 * 
 * \param tDe : Tableau avec les scores des dés lancés
 * 
 * \return vrai si les dés obtenus forment une grande suite, faux sinon
 * 
 * Cherche dans le tableau des dés la présence d'une grande suite et retourne si oui ou non il y a une grande suite
 * 
 */
bool grandeSuite(tabDe tDe) {

    bool trouve;
    int i, deEtud, deActuel, avance;

    triDe(tDe);

    trouve = false;
    i = 1;
    deActuel = tDe[0];
    avance = 1;

    while(i < NB_DE && trouve == false) {

        deEtud = tDe[i];

        if(deActuel + 1 == deEtud) {

            deActuel = tDe[i];
            avance = avance + 1;

        }

        if(avance == 5) {

            trouve = true;

        }

        i = i + 1;

    }

    return trouve;

}

/**
 * 
 * \fn int yams(tabDe tDe)
 * 
 * \brief Vérifie si les dés obtenu forme un yams
 * 
 * \param tDe : Tableau avec les scores des dés lancés
 * 
 * \return Somme des 5 dés formant le yams
 * 
 * Cherche dans le tableau des dés si il y a un yams et retourne le score obtenu (si il n'y a pas de yams, on retourne -1)
 * 
 */
int yams(tabDe tDe) {

    int i, verif, nbDe, score;
                
    verif = tDe[0];
    nbDe = 1;

    for(i = 1 ; i < NB_DE ; i++) {

        if (verif == tDe[i]) {

            verif = tDe[i];
            nbDe = nbDe + 1;

        }

    }

    if (nbDe == 5) {

        score = 5 * tDe[0];

    } else {

        score = -1;

    }

    return score;

}

/**
 * 
 * \fn void affecteScore(tabScore tscore, tabDe tDe, char joueurActuel[10])
 * 
 * \brief Demande au joueur en train de joueur où est-ce qu'il souhaite affecté le score qu'il a obtenu
 * 
 * \param tscore : Tableau des scores du joueur actuellement en train de jouer
 * 
 * \param tDe : Tableau des dés obtenus par le joueur actuel
 * 
 * \param joueurActuel : Nom du joueur actuel
 * 
 * Demande au joueur où est ce qu'il souhaite affecté son score et affiche son tableau des scores avec ses scores précédents
 * 
 */
void affecteScore(tabScore tscore, tabDe tDe, char joueurActuel[10]){
    int choix, somme;
    bool jouer, trouve;

    jouer = false;

    afficheTableau1Joueur(tscore, joueurActuel);

    somme = afficheSomme(tDe);

    while (jouer == false) {

        printf("Où souhaitez vous affecté votre score ? Saisir le nombre correspondant à la case (Colonne de gauche du tableau)\n");
        scanf("%d", &choix);

        switch (choix) {
/*Somme de 1*/
            case 1:
                if (tscore[0] == 0){   

                    tscore[0] = scoreSomme(tDe, 1);
                    jouer = true;

                } else {

                    printf("Case déjà remplie ! Rentrez le score dans une autre case");

                }
                break;
/*Somme de 2*/
            case 2:
                if (tscore[1] == 0){   

                    tscore[1] = scoreSomme(tDe, 2);
                    jouer = true;

                } else {

                    printf("Case déjà remplie ! Rentrez le score dans une autre case");

                }
                break;
/*Somme de 3*/           
            case 3:
                if (tscore[2] == 0){   

                    tscore[2] = scoreSomme(tDe, 3);
                    jouer = true;

                } else {

                    printf("Case déjà remplie ! Rentrez le score dans une autre case");

                }
                break;
/*Somme de 4*/           
            case 4:
                if (tscore[3] == 0){   

                    tscore[3] = scoreSomme(tDe, 4);
                    jouer = true;

                } else {

                    printf("Case déjà remplie ! Rentrez le score dans une autre case");

                }
                break;
/*Somme de 5*/            
            case 5:
                if (tscore[4] == 0){   

                    tscore[4] = scoreSomme(tDe, 5);
                    jouer = true;

                } else {

                    printf("Case déjà remplie ! Rentrez le score dans une autre case");

                }
                break;
/*Somme de 6*/
            case 6:
                if (tscore[5] == 0){   

                    tscore[5] = scoreSomme(tDe, 6);
                    jouer = true;

                } else {

                    printf("Case déjà remplie ! Rentrez le score dans une autre case");

                }
                break;
/*Brelan*/
            case 7:
                if (tscore[8] == 0) {

                    tscore[8] = brelan(tDe);
                    jouer = true;

                } else {

                    printf("Case déjà remplie ! Rentrez le score dans une autre case");

                }
                break;
/*Carré*/
            case 8:
                if (tscore[9] == 0) {

                    tscore[9] = carre(tDe);
                    jouer = true;

                } else {

                    printf("Case déjà remplie ! Rentrez le score dans une autre case");

                }
                break;
/*Full House*/
            case 9:
                if (tscore[10] == 0) {

                    trouve = fullHouse(tDe);

                    if(trouve == true) {

                        tscore[10] = 25;

                    } else {

                        tscore[10] = -1;

                    }

                    jouer = true;

                } else {

                    printf("Case déjà remplie ! Rentrez le score dans une autre case");

                }
                break;
/*Petite Suite*/
            case 10:

                if (tscore[11] == 0) {

                    trouve = petiteSuite(tDe);

                    if (trouve == true) {

                        tscore[11] = 30;

                    } else {

                        tscore[11] = -1;

                    }

                    jouer = true;

            } else {

                printf("Case déjà remplie ! Rentrez le score dans une autre case");

            }
                break;
/*Grande Suite*/
            case 11:
                if (tscore[12] == 0) {

                    trouve = grandeSuite(tDe);

                    if  (trouve == true) {

                        tscore[12] = 40;

                    } else {

                        tscore[12] = -1;

                    }

                    jouer = true;

                } else {

                    printf("Case déjà remplie ! Rentrez le score dans une autre case");

                }
                    break;
/*Yams*/
            case 12:

                if (tscore[13] == 0) {

                    tscore[13] = yams(tDe);
                    jouer = true;

                } else {

                    printf("Case déjà remplie ! Rentrez le score dans une autre case");

                }

                break;
/*Chance*/
            case 13:
                if (tscore[14] == 0) {

                    tscore[14] = somme;
                    jouer = true;

                } else {

                    printf("Case déjà remplie ! Rentrez le score dans une autre case");

                }
                break;

        }

    }

}

/**
 * 
 * \fn void calculTotal(tabScore tscore)
 * 
 * \brief Calcule les différents totaux (inférieur, supérieur et général) et le bonus
 * 
 * \param tscore : Tableau des scores du joueur actuellement en train de jouer
 * 
 * Calcule les différents totaux du tableau et le bonus et les affecte aux cases dédiées du tableau
 * 
 */
void calculTotal(tabScore tscore) {

    tscore[7] = tscore[0] + tscore[1] + tscore[2] + tscore[3] + tscore[4] + tscore[5];

    if(tscore[7] > 62) {

        tscore[6] = 35;

    }

    tscore[15] = tscore[8] + tscore[9] + tscore[10] + tscore[11] + tscore[12] + tscore[13] + tscore[14];

    tscore[16] = tscore[7] + tscore[15];

}

/**
 * 
 * \fn void afficheGagnant(tabScore tscore1, tabScore tscore2, char j1[10], char j2[10])
 * 
 * \brief Affiche le tableau de tous les scores de tous les joueurs et le gagnant 
 * 
 * \param tscore1 : Tableau des scores du joueur 1
 * 
 * \param tscore2 : Tableau des scores du joueur 2
 * 
 * \param j1 : Nom du joueur 1
 * 
 * \param j2 : Nom du joueur 2
 * 
 * Affiche tous les scores des joueurs et le nom du gagnant de la partie donc celui qui a le score total le plus élevé
 * 
 */
void afficheGagnant(tabScore tscore1, tabScore tscore2, char j1[10], char j2[10]) {

    calculTotal(tscore1);
    calculTotal(tscore2);

    afficheTableau(tscore1, tscore2, j1, j2);

    if (tscore1[16] > tscore2[16]) {

        printf("Félicitation %s ! Vous avez gagné avec %d points!", j1, tscore1[16]);

    } else {

        printf("Félicitation %s ! Vous avez gagné avec %d points!", j2, tscore2[16]);

    }

}

/**
 * 
 * \fn void initTabVide(tabScore tscore)
 * 
 * \brief Initialise toute les cases où le score est de -1
 * 
 * \param tscore : Tableau des scores du joueur actuellement en train de jouer
 * 
 * Initialise les cases vides du tableau, donc les cases où la valeur -1 est saisies afin de ne pas fausser les totaux de fin de partie
 * 
 */
void initTabVide(tabScore tscore) {

    int i;

    for(i = 0;i < NB_CASE; i++) {

        if (tscore[i] == -1) { 

            tscore[i] = 0;

        }

    }

}

/**
 * 
 * \fn int main()
 * 
 * \brief Programme principal
 * 
 * Il s'agit du programme principal, le coeur du programme
 * 
 */
int main() {

    int nbTour, joueur;
    char j1[10], j2[10], joueurActuel[10];
    tabScore tscore1, tscore2;
    tabDe tDe, tGarde;

    initTabScore(tscore1);
    initTabScore(tscore2);
    initTabDe(tDe);
    initTabDe(tGarde);

    printf("Bienvenue sur le jeu du Yams\n");

    nomJoueur(j1, j2);

    for (nbTour = 1; nbTour <= 13; nbTour = nbTour + 1) {

        for (joueur = 1; joueur <= NB_JOUEUR; joueur = joueur + 1) {

            if (joueur == 1) {
                strcpy(joueurActuel, j1);
            } else {
                strcpy(joueurActuel, j2);
            }
            
            afficheTableau(tscore1, tscore2, j1, j2);

            propositionJeu(joueurActuel);

            lanceDe(joueurActuel, tDe, tGarde);

            if (joueur == 1) {
                affecteScore(tscore1, tDe, joueurActuel);
                calculTotal(tscore1);
            } else {
                affecteScore(tscore2, tDe, joueurActuel);
                calculTotal(tscore2);
            }

        }

    }

    initTabVide(tscore1);
    initTabVide(tscore2);

    afficheGagnant(tscore1, tscore2, j1, j2);

    return EXIT_SUCCESS;

}